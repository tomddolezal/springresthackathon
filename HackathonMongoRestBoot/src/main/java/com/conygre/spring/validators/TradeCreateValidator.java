package com.conygre.spring.validators;

import com.conygre.spring.entities.Trade;

import com.conygre.spring.entities.TradeState;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.logging.Logger;

@Component("beforeCreateTradeValidator")
public class TradeCreateValidator implements Validator {

    private static final Logger logger = Logger.getLogger(TradeCreateValidator.class.getName());

    @Override
    public boolean supports(Class<?> trade) {
        return Trade.class.equals(trade);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Trade trade = (Trade) target;
        trade.setDate(LocalDateTime.now().toString());
        trade.setStatus(TradeState.PROCESSING);

    }
}
