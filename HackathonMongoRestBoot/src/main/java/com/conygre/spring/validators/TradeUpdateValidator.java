package com.conygre.spring.validators;

import com.conygre.spring.HackathonMongoRestBootApplication;
import com.conygre.spring.data.TradeRepository;
import com.conygre.spring.entities.Trade;
import com.conygre.spring.entities.TradeState;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.logging.Logger;

@Component("beforeSaveTradeValidator")
public class TradeUpdateValidator implements Validator {

    private static final Logger logger = Logger.getLogger(TradeUpdateValidator.class.getName());

    @Override
    public boolean supports(Class<?> trade) {
        return Trade.class.equals(trade);
    }

    @Override
    public void validate(Object target, Errors errors) {
        // New object to replace with
        Trade newTrade = (Trade) target;

        // Pull repo from context
        TradeRepository tr = HackathonMongoRestBootApplication.context.getBean(TradeRepository.class);

        // Existing object
        Trade curTrade;

        if (!tr.findById(newTrade.getId()).isPresent()) {
            errors.rejectValue("id", "0", "Trade not Found!");
        }

        curTrade = tr.findById(newTrade.getId()).get();
        if (curTrade.getStatus() != TradeState.CREATED && curTrade.getStatus() != TradeState.PROCESSING) {
            errors.rejectValue("id", "1", "Trade can not be changed!");
        }

    }

}
