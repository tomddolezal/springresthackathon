package com.conygre.spring.entities;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
public class Trade {

    @Id
    @JsonSerialize(using = TradeIdSerializer.class)
    private ObjectId id;

    private String date;
    private final String ticker;
    private final int quantity;
    private final int price;
    private TradeState status;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Trade(String date, String ticker, int quantity, int price, TradeState status) {
        this.date = date;
        this.ticker = ticker;
        this.quantity = quantity;
        this.price = price;
        this.status = status;
    }

    public String getTicker() {
        return ticker;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getPrice() {
        return price;
    }

    public TradeState getStatus() {
        return status;
    }

    public void setStatus(TradeState state) {
        this.status = state;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
