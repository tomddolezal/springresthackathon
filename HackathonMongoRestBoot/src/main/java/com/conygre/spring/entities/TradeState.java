package com.conygre.spring.entities;

public enum TradeState {

    CREATED("CREATED"),
    PROCESSING("PROCESSING"),
    FILLED("FILLED"),
    REJECTED("REJECTED");

    private final String state;

    private TradeState(String state) {
        this.state = state;
    }

    public String getState() {
        return this.state;
    }

}
