package com.conygre.spring.entities;

import com.mongodb.lang.Nullable;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.HashIndexed;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class StockDetail {
    @Id
    private ObjectId id;

    @HashIndexed
    private String ticker;

    private String name;

    @Nullable
    private boolean is_etf;

    private double price;

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isIs_etf() {
        return is_etf;
    }

    public void setIs_etf(boolean is_etf) {
        this.is_etf = is_etf;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
