package com.conygre.spring.data;

import com.conygre.spring.entities.Trade;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "trade", path = "tradeapi")
public interface TradeRepository extends MongoRepository<Trade, ObjectId>{

    List<Trade> findByTicker(@Param("ticker") String ticker);

}
