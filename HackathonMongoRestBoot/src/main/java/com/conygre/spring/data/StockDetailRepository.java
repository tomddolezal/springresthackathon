package com.conygre.spring.data;

import com.conygre.spring.entities.StockDetail;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StockDetailRepository extends MongoRepository<StockDetail, ObjectId> {
}
