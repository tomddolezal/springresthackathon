package com.conygre.spring.rest;

import com.conygre.spring.entities.Trade;
import com.conygre.spring.service.TradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.bson.types.ObjectId;

@RestController
@RequestMapping("/trade")
@CrossOrigin // allows requests from all domains
public class TradeController {

    private final TradeService service;

    public TradeController(@Autowired TradeService service) {
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Trade> findAll() {
        // logger.info("managed to call a Get request for findAll");
        return service.getTrades();
    }

    @RequestMapping(method = RequestMethod.POST)
    public void addTrade(@RequestBody Trade trade) {
        service.addTrade(trade);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteCd(@PathVariable("id") String id) {
        service.deleteTrade(new ObjectId(id));
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteCd(@RequestBody Trade disc) {
        service.deleteTrade(disc);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Trade updateTrade(@RequestBody Trade trade) {
        return service.updateTrade(trade);
    }

}
