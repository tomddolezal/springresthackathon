package com.conygre.spring.rest;

import com.conygre.spring.entities.Portfolio;
import com.conygre.spring.entities.Trade;
import com.conygre.spring.service.PortfolioService;
import com.conygre.spring.service.TradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PortfolioController {
    private final PortfolioService service;

    public PortfolioController(@Autowired PortfolioService service) {
        this.service = service;
    }

    @RequestMapping(path = "/portfolio", method = RequestMethod.GET)
    public Iterable<Portfolio> findAll() {
        service.refresh();
        return service.getPortfolio();
    }

    @RequestMapping(path = "/portfoliorefresh")
    public void refresh() {
        service.refresh();
    }
}
