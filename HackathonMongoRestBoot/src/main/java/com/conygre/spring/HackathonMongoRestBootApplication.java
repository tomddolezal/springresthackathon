package com.conygre.spring;

import com.conygre.spring.validators.TradeCreateValidator;
import com.conygre.spring.validators.TradeUpdateValidator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@SpringBootApplication
@Configuration
@EnableMongoRepositories(basePackages = "com.conygre.spring.data")
public class HackathonMongoRestBootApplication implements RepositoryRestConfigurer {

	public static ApplicationContext context;

    public static void main(String[] args) {
		context = SpringApplication.run(HackathonMongoRestBootApplication.class, args);
    }

	@Override
	public void configureValidatingRepositoryEventListener(
			ValidatingRepositoryEventListener v) {
		v.addValidator("beforeSave", new TradeUpdateValidator());
		v.addValidator("beforeCreate", new TradeCreateValidator());
	}


}
