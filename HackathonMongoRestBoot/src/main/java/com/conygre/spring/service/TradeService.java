package com.conygre.spring.service;

import com.conygre.spring.entities.Trade;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.util.Collection;


public interface TradeService {

    Collection<Trade> getTrades();
    void addTrade(Trade trade);

    void deleteTrade(ObjectId id);
    void deleteTrade(Trade trade);


    Trade updateTrade(Trade trade);
}
