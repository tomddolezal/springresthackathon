package com.conygre.spring.service;

import com.conygre.spring.entities.Portfolio;
import org.springframework.stereotype.Service;
import java.util.Collection;


@Service
public interface PortfolioService {

    Collection<Portfolio> getPortfolio();

    void refresh();

    void refreshByTicker(String ticker);
}
