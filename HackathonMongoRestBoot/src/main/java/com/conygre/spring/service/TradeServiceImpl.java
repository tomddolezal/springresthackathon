package com.conygre.spring.service;

import com.conygre.spring.data.TradeRepository;
import com.conygre.spring.entities.Trade;
import com.conygre.spring.entities.TradeState;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class TradeServiceImpl implements TradeService {

    private final TradeRepository dao;

    public TradeServiceImpl(@Autowired TradeRepository dao) {
        this.dao = dao;
    }

    @Override
    public Collection<Trade> getTrades() {
        return dao.findAll();
    }

    @Override
    public void addTrade(Trade trade) {
        dao.insert(trade);
    }

    @Override
    public void deleteTrade(ObjectId id) {
        dao.deleteById(id);
    }

    @Override
    public void deleteTrade(Trade trade) {
        dao.delete(trade);
    }

    @Override
    public Trade updateTrade(Trade trade) {
        return dao.findById(trade.getId())
                .filter(value ->
                        value.getStatus() == TradeState.CREATED)
                .map(value -> dao.save(trade))
                .orElse(null);
    }
}
