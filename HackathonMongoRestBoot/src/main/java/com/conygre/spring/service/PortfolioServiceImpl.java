package com.conygre.spring.service;

import com.conygre.spring.data.PortfolioRepository;
import com.conygre.spring.data.TradeRepository;
import com.conygre.spring.entities.Portfolio;
import com.conygre.spring.entities.Trade;
import com.conygre.spring.entities.TradeState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.TypedAggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Service
public class PortfolioServiceImpl implements PortfolioService{

    Logger logger = LoggerFactory.getLogger(PortfolioServiceImpl.class);

    private final PortfolioRepository pdao;

    private final TradeRepository tdao;

    private final MongoTemplate mongoTemplate;



    public PortfolioServiceImpl(@Autowired PortfolioRepository pdao, @Autowired TradeRepository tdao, @Autowired MongoTemplate mongoTemplate) {
        this.pdao = pdao;
        this.tdao = tdao;
        this.mongoTemplate = mongoTemplate;
    }

    public Collection<Portfolio> getPortfolio() {
        return pdao.findAll();
    }

    public void refresh(){
        refreshHelper(null);
    }

    public void refreshByTicker(String ticker){
        refreshHelper(ticker);
    }

    /**
     * Help to refresh the portfolio
     * @param ticker String Ticker. Null for wildcard
     */
    private void refreshHelper(String ticker){
        List<AggregationOperation> list = new ArrayList<>();
        // Filter by status
        list.add(Aggregation.match(Criteria.where("status").is(TradeState.FILLED)));

        if (ticker != null){
            list.add(Aggregation.match(Criteria.where("ticker").is(ticker)));
        }
        // Add col amount = price * quantity
        list.add(Aggregation
                .project("ticker", "price", "quantity")
                .and("price")
                .multiply("quantity")
                .as("amount"));

        // Group and get sums (totalQuantity and totalAmount)
        list.add(Aggregation
                .group("ticker")
                .sum("quantity").as("totalQuantity")
                .sum("amount").as("totalAmount")
                .first("ticker").as("ticker"));

        // Remove ids, since we will cast Grouped Trade object to Portfolio. The id makes no sense.
        list.add(Aggregation
                .project("ticker", "totalQuantity", "totalAmount")
                .andExclude("_id"));

        // Ignore closed position
        list.add(Aggregation.match(Criteria.where("totalQuantity").ne(0)));

        // Join Stock Details
        list.add(Aggregation.lookup("stockdetail","ticker", "ticker", "info" ));

        // Get details and current price from Stock Details collection
        list.add(Aggregation.project("ticker", "totalQuantity", "totalAmount")
                .and("info.name").as("name")
                .and("info.price").as("currentPrice"));

        list.add(Aggregation.sort(Sort.Direction.ASC, "ticker"));

        List<Portfolio> result;

        // Build aggregate query on Trade
        TypedAggregation<Trade> agg = Aggregation.newAggregation(Trade.class, list);
        // Map the result to Portfolio object
        result = mongoTemplate.aggregate(agg, Portfolio.class).getMappedResults();

        // Since
        pdao.deleteAll();
        pdao.insert(result);
    }
}
