//////////////
/// Chart ////
//////////////


var API_KEY = "WSGSSRJ0ZQ9O7NEG";

var ctx = document.getElementById('chart').getContext('2d');

ctx.canvas.width = 650;
ctx.canvas.height = 400;

var current_data = [];
var current_ticker = "SPY";
var current_ticker_desc = "";
var chart;
var interval = "5min";
var latest_price;
var price_cache = [];



$( document ).ready(function(){
	// Load Portfolio from API
	setInterval(function(){getPPortfolio()}, 5000);
	getPPortfolio();
	showChart();
})

function getPPortfolio(){
	// Load Portfolio from API
	$.get('/portfolio', function(raw_data){
		$("#p-list").empty();
		Object.keys(raw_data).forEach(key => {
			let elem = raw_data[key];
			let ticker = elem["ticker"];
			let pl = (elem["totalQuantity"]* price_cache[ticker] - elem["totalAmount"]).toFixed(2);
			$("#p-list").append(' <div class="p-item" data-ticker="'+ticker+'" onclick="showChartFromPortfolio(\''+ticker+'\')">\n' +
				'<div class="p-row clearfix">\n' +
				'    <span class="p-item-ticker float-left">'+ticker+'</span>\n' +
				'    <span class="p-item-price float-right ">'+elem["currentPrice"]+'</span>\n' +
				'    <span class="p-item-quantity float-right">'+elem["totalQuantity"]+'</span>\n' +
				'</div>\n' +
				'<div class="p-row clearfix">\n' +
				'    <span class="p-item-desc float-left">'+elem["name"]+'</span>\n' +
				'    <span class="p-item-pl float-right" style="color:'+ ((pl > 0)? 'green':'red') + '">'+ pl +'</span>\n' +
				'</div>\n' +
				'</div>');
		});

		// Update portfolio highlight
		$(".p-item").removeClass('active-item');
		$(document).find(".p-item[data-ticker='"+current_ticker+"']").addClass('active-item');
	});
}

// Switch to new ticker and reload chart
function showChartFromPortfolio(ticker){
	if (current_ticker == ticker){
		return;
	}

	current_ticker = ticker;
	showChart();
}

// Reload chart onchange
function showChart(){
	if (current_ticker === undefined){
		console.log("Undefined Ticker");
		return;
	}

	$('#chart-loader').show();
	$('#chart').hide();

	var url = "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol="+current_ticker+"&interval="+interval+"&apikey=" + API_KEY;
	$.get(url, function(raw_data){
		process_data(raw_data);

		// New chart
		if (chart == null){
			chart = new Chart(ctx, {
				type: 'candlestick',
				data: {
					datasets: [{
						label: current_ticker,
						data: current_data
					}]
				}
			});

		} else {
			// Update / Refresh Chart
			chart.data.datasets[0].data = current_data;
			chart.data.datasets[0].label = current_ticker;
			chart.update();
		}
		$('#chart-loader').hide();
		$('#chart').show();
		$('#modalTicker1, #modalTicker3').val(current_ticker);


	});

}

// Process Alpha Vantage data to Chart.js data
function process_data(raw_data){
	let real_data = raw_data[Object.keys(raw_data)[1]];

	if (real_data === undefined){
		if(raw_data["Error Message"] !== undefined){
			alert(raw_data["Error Message"]);
		} else {
			alert(raw_data["Note"]);
		}
		return;
	}
	current_data = [];
	Object.keys(real_data).forEach(key => {
		let elem = real_data[key];
		date = luxon.DateTime.fromFormat(key, "yyyy-mm-dd hh:mm:ss");
		current_data.push({
			t: date.valueOf(),
			o: elem['1. open'],
			h: elem['2. high'],
			l: elem['3. low'],
			c: elem['4. close']
		});
	});
	current_data = current_data.reverse();
	latest_price = current_data[0].c;
	price_cache[current_ticker] = latest_price;
}


// Callback for chart option update
var update = function() {
	var dataset = chart.config.data.datasets[0];

	// candlestick vs ohlc
	var type = document.getElementById('type').value;
	dataset.type = type;

	interval = document.getElementById('interval').value;

	showChart();
};


//////////////
/// Search ///
//////////////

// Auto complete
var ticker_dic = ["SPY", "QQQ", "A","AA","AABA","AAC","AAL","AAMC","AAME","AAN","AAOI","AAON","AAP","AAPL","AAT","AAU","AAWW","AAXJ","AAXN","AB","ABB","ABBV","ABC","ABCB","ABDC","ABEO","ABEOW","ABEV","ABG","ABIL","ABIO","ABM","ABMD","ABR","ABR^A","ABR^B","ABR^C","ABT","ABTX","ABUS","AC","ACA","ACAD","ACAM","ACAMU","ACAMW","ACB","ACBI","ACC","ACCO","ACCP","ACER","ACGL","ACGLO","ACGLP","ACH","ACHC","ACHN","ACHV","ACIA","ACIU","ACIW","ACLS","ACM","ACMR","ACN","ACNB","ACOR","ACP","ACRE","ACRS","ACRX","ACST","ACT","ACTG","ACTT","ACTTU","ACTTW","ACU","ACV","ACWI","ACWX","ACY","ADAP","ADBE","ADC","ADES","ADI","ADIL","ADILW","ADM","ADMA","ADMP","ADMS","ADNT","ADP","ADPT","ADRA","ADRD","ADRE","ADRO","ADRU","ADS","ADSK","ADSW","ADT","ADTN","ADUS","ADVM","ADX","ADXS","AE","AEB","AEE","AEF","AEG","AEGN","AEH","AEHR","AEIS","AEL","AEM","AEMD","AEO","AEP","AEP^B","AER","AERI","AES","AESE","AEY","AEYE","AEZS","AFB","AFC","AFG","AFGB","AFGE","AFGH","AFH","AFHBL","AFI","AFIN","AFINP","AFL","AFMD","AFT","AFYA","AG","AGBA","AGBAR","AGBAU","AGBAW","AGCO","AGD","AGE","AGEN","AGFS","AGFSW","AGI","AGIO","AGLE","AGM","AGM.A","AGMH","AGM^A","AGM^C","AGM^D","AGN","AGNC","AGNCB","AGNCM","AGNCN","AGND","AGO","AGO^B","AGO^E","AGO^F","AGR","AGRO","AGRX","AGS","AGTC","AGX","AGYS","AGZD","AHC","AHH","AHH^A","AHL^C","AHL^D","AHL^E","AHPI","AHT","AHT^D","AHT^F","AHT^G","AHT^H","AHT^I","AI","AIA","AIC","AIF","AIG","AIG.WS","AIG^A","AIHS","AIM","AIMC","AIMT","AIN","AINC","AINV","AIQ","AIR","AIRG","AIRI","AIRR","AIRT","AIRTP","AIRTW","AIT","AIV","AIW","AIZ","AIZP","AI^B","AI^C","AJG","AJRD","AJX","AJXA","AKAM","AKBA","AKCA","AKER","AKG","AKO.A","AKO.B","AKR","AKRO","AKRX","AKS","AKTS","AKTX","AL","ALAC","ALACR","ALACU","ALACW","ALB","ALBO","ALC","ALCO","ALDR","ALDX","ALE","ALEC","ALEX","ALG","ALGN","ALGR","ALGRR","ALGRU","ALGRW","ALGT","ALIM","ALIT","ALJJ","ALK","ALKS","ALL","ALLE","ALLK","ALLO","ALLT","ALLY","ALLY^A","ALL^A","ALL^B","ALL^D.CL","ALL^E.CL","ALL^F.CL","ALL^G","ALL^H","ALNA","ALNY","ALO","ALOT","ALPN","ALP^Q","ALRM","ALRN","ALRS","ALSK","ALSN","ALT","ALTM","ALTR","ALTY","ALV","AL^A","APEN","APEX","APH","APHA","APLE","APLS","APLT","APM","APO","APOG","APOP","APOPW","APO^A","APO^B","APPF","APPN","APPS","APRN","APT","APTO","APTS","APTV","APTX","APVO","APWC","APY","APYX","AQ","AQB","AQMS","AQN","AQNA","AQNB","AQST","AQUA","AR","ARA","ARAV","ARAY","ARC","ARCB","ARCC","ARCE","ARCH","ARCO","ARCT","ARD","ARDC","ARDS","ARDX","ARE","AREC","ARES","ARES^A","AREX","ARE^D","ARGD","ARGO","ARGX","ARI","ARKR","ARL","ARLO","ARLP","ARMK","ARMP","ARNA","ARNC","ARNC^","AROC","AROW","ARPO","ARQL","ARR","ARR^B","ARTL","ARTLW","ARTNA","ARTW","ARTX","ARVN","ARW","ARWR","ARYA","ARYAU","ARYAW","ASA","ASB","ASB^C","ASB^D","ASB^E","ASC","ASET","ASFI","ASG","ASGN","ASH","ASIX","ASLN","ASM","ASMB","ASML","ASNA","ASND","ASPN","ASPS","ASPU","ASR","ASRT","ASRV","ASRVP","ASTC","ASTE","ASUR","ASX","ASYS","AT","ATAI","ATAX","ATEC","ATEN","ATEST","ATEST.A","ATEST.B","ATEST.C","ATEX","ATGE","ATH","ATHE","ATHM","ATHX","ATH^A","ATI","ATIF","ATIS","ATISW","ATKR","ATLC","ATLO","ATNI","ATNM","ATNX","ATO","ATOM","ATOS","ATR","ATRA","ATRC","ATRI","ATRO","ATRS","ATSG","ATTO","ATU","ATUS","ATV","ATVI","ATXI","AU","AUB","AUBN","AUDC","AUG","AUMN","AUO","AUPH","AUTL","AUTO","AUY","AVA","AVAL","AVAV","AVB","AVCO","AVD","AVDL","AVDR","AVEO","AVGO","AVGR","AVH","AVID","AVK","AVLR","AVNS","AVNW","AVP","AVRO","AVT","AVTR","AVTR^A","AVX","AVXL","AVY","AVYA","AWF","AWI","AWK","AWP","AWR","AWRE","AWSM","AWX","AX","AXAS","AXDX","AXE","AXGN","AXGT","AXL","AXLA","AXNX","AXO","AXP","AXR","AXS","AXSM","AXS^D","AXS^E","AXTA","AXTI","AXU","AY","AYI","AYR","AYTU","AYX","AZN","AZO","AZPN","AZRE","AZRX","AZUL","AZZ","B","BA","BABA","BAC","BAC^A","BAC^B","BAC^C","BAC^E","BAC^K","BAC^L","BAC^M","BAC^Y","BAF","BAH","BAM","BANC","BANC^D","BANC^E","BAND","BANF","BANFP","BANR","BANX","BAP","BAS","BASI","BATRA","BATRK","BAX","BB","BBAR","BBBY","BBC","BBCP","BBD","BBDC","BBDO","BBF","BBGI","BBH","BBI","BBIO","BBK","BBL","BBN","BBP","BBRX","BBSI","BBT","BBT^F","BBT^G","BBT^H","BBU","BBVA","BBW","BBX","BBY","BC","BCBP","BCC","BCDA","BCDAW","BCE","BCEI","BCEL","BCH","BCLI","BCML","BCNA","BCO","BCOM","BCOR","BCOV","BCOW","BCPC","BCRH","BCRX","BCS","BCSF","BCTF","BCV","BCV^A","BCX","BCYC","BC^A","BC^B","BC^C","BDC","BDGE","BDJ","BDL","BDN","BDR","BDSI","BDX","BDXA","BE","BEAT","BECN","BEDU","BELFA","BELFB","BEN","BEP","BERY","BEST","BF.A","BF.B","BFAM","BFC","BFIN","BFIT","BFK","BFO","BFRA","BFS","BFST","BFS^C","BFS^D","BFY","BFZ","BG","BGB","BGCP","BGFV","BGG","BGH","BGI","BGIO","BGNE","BGR","BGRN","BGS","BGSF","BGT","BGX","BGY","BH","BH.A","BHAT","BHB","BHC","BHE","BHF","BHFAL","BHFAP","BHGE","BHK","BHLB","BHP","BHR","BHR^B","BHR^D","BHTG","BHV","BHVN","BIB","BICK","BID","BIDU","BIF","BIG","BIIB","BILI","BIMI","BIO","BIO.B","BIOC","BIOL","BIOS","BIOX","BIOX.WS","BIP","BIS","BIT","BITA","BIVI","BJ","BJRI","BK","BKCC","BKCH","BKD","BKE","BKEP","BKEPP","BKH","BKI","BKJ","BKK","BKN","BKNG","BKSC","BKT","BKTI","BKU","BKYI","BK^C","BL","BLBD","BLCM","BLCN","BLD","BLDP","BLDR","BLE","BLFS","BLIN","BLK","BLKB","BLL","BLMN","BLNK","BLNKW","BLPH","BLRX","BLU","BLUE","BLW","BLX","BMA","BMCH","BME","BMI","BMLP","BML^G","BML^H","BML^J","BML^L","BMO","BMRA","BMRC","BMRN","BMTC","BMY","BND","BNDW","BNDX","BNED","BNFT","BNGO","BNGOW","BNKL","BNS","BNSO","BNTC","BNTCW","BNY","BOCH","BOE","BOH","BOKF","BOKFL","BOLD","BOMN","BOOM","BOOT","BORR","BOSC","BOTJ","BOTZ","BOX","BOXL","BP","BPFH","BPL","BPMC","BPMP","BPMX","BPOP","BPOPM","BPOPN","BPR","BPRAP","BPRN","BPT","BPTH","BPY","BPYPO","BPYPP","BQH","BR","BRC","BREW","BRFS","BRG","BRG^A","BRG^C","BRG^D","BRID","BRK.A","BRK.B","BRKL","BRKR","BRKS","BRN","BRO","BROG","BROGR","BROGU","BROGW","BRPA","BRPAR","BRPAU","BRPAW","BRPM","BRPM.U","BRPM.WS","BRQS","BRT","BRX","BRY","BSA","BSAC","BSBR","BSD","BSE","BSET","BSGM","BSIG","BSL","BSM","BSMX","BSQR","BSRR","BST","BSTC","BSTZ","BSVN","BSX","BT","BTA","BTAI","BTE","BTEC","BTG","BTI","BTN","BTO","BTT","BTU","BTZ","BUD","BUI","BURG","BURL","BUSE","BV","BVN","BVSN","BVXV","BVXVW","BW","BWA","BWAY","BWB","BWEN","BWFG","BWG","BWL.A","BWMC","BWMCU","BWMCW","BWXT","BX","BXC","BXG","BXMT","BXMX","BXP","BXP^B","BXS","BY","BYD","BYFC","BYM","BYND","BYSI","BZH","BZM","BZUN","C","CAAP","CAAS","CABO","CAC","CACC","CACG","CACI","CADE","CAE","CAF","CAG","CAH","CAI","CAI^A","CAI^B","CAJ","CAKE","CAL","CALA","CALM","CALX","CAMP","CAMT","CANF","CANG","CAPL","CAPR","CAR","CARA","CARB","CARE","CARG","CARO","CARS","CART","CARV","CARZ","CASA","CASH","CASI","CASS","CASY","CAT","CATB","CATC","CATH","CATM","CATO","CATS","CATY","CB","CBAN","CBAT","CBAY","CBB","CBB^B","CBD","CBFV","CBH","CBIO","CBL","CBLI","CBLK","CBL^D","CBL^E","CBM","CBMB","CBMG","CBNK","CBO","CBOE","CBPO","CBPX","CBRE","CBRL","CBS","CBS.A","CBSH","CBSHP","CBT","CBTX","CBU","CBUS","CBX","CFG","CFG^D","CFMS","CFO","CFR","CFRX","CFR^A","CFX","CFXA","CG","CGA","CGBD","CGC","CGEN","CGIX","CGNX","CGO","CHA","CHAC","CHAC.U","CHAC.WS","CHAP","CHCI","CHCO","CHCT","CHD","CHDN","CHE","CHEF","CHEK","CHEKW","CHEKZ","CHFS","CHGG","CHH","CHI","CHIC","CHK","CHKP","CHKR","CHK^D","CHL","CHMA","CHMG","CHMI","CHMI^A","CHMI^B","CHN","CHNA","CHNG","CHNGU","CHNR","CHRA","CHRS","CHRW","CHS","CHSCL","CHSCM","CHSCN","CHSCO","CHSCP","CHSP","CHT","CHTR","CHU","CHUY","CHW","CHWY","CHY","CI","CIA","CIB","CIBR","CID","CIDM","CIEN","CIF","CIFS","CIG","CIG.C","CIGI","CIH","CII","CIK","CIL","CIM","CIM^A","CIM^B","CIM^C","CIM^D","CINF","CINR","CIO","CIO^A","CIR","CISN","CIT","CIVB","CIVBP","CIX","CIZ","CIZN","CJ","CJJD","CKH","CKPT","CKX","CL","CLAR","CLB","CLBK","CLBS","CLCT","CLDB","CLDR","CLDT","CLDX","CLF","CLFD","CLGN","CLGX","CLH","CLI","CLIR","CLLS","CLM","CLMT","CLNC","CLNE","CLNY","CLNY^B","CLNY^E","CLNY^G","CLNY^H","CLNY^I","CLNY^J","CLOU","CLPR","CLPS","CLR","CLRB","CLRBZ","CLRG","CLRO","CLS","CLSD","CLSN","CLUB","CLVS","CLW","CLWT","CLX","CLXT","CM","CMA","CMBM","CMC","CMCL","CMCM","CMCO","CMCSA","CMCT","CMCTP","CMD","CME","CMFNL","CMG","CMI","CMLS","CMO","CMO^E","CMP","CMPR","CMRE","CMRE^B","CMRE^C","CMRE^D","CMRE^E","CMRX","CMS","CMSA","CMSC","CMSD","CMS^B","CMT","CMTL","CMU","CNA","CNAT","CNBKA","CNC","CNCE","CNCR","CNDT","CNET","CNF","CNFR","CNFRL","CNHI","CNI","CNK","CNMD","CNNE","CNO","CNOB","CNP","CNP^B","CNQ","CNR","CNS","CNSL","CNST","CNTF","CNTX","CNTY","CNX","CNXM","CNXN","CO","COCP","CODA","CODI","CODI^A","CODI^B","CODX","COE","COF","COF^C","COF^D","COF^F","COF^G","COF^H","COF^P","COG","COHN","COHR","COHU","COKE","COLB","COLD","COLL","COLM","COMM","COMT","CONE","CONN","COO","COOP","COP","COR","CORE","CORR","CORR^A","CORT","CORV","COST","COT","COTY","COUP","COWN","COWNL","COWNZ","CP","CPA","CPAA","CPAAU","CPAAW","CPAC","CPAH","CPB","CPE","CPF","CPG","CPHC","CPHI","CPIX","CPK","CPL","CPLG","CPLP","CPRI","CPRT","CPRX","CPS","CPSH","CPSI","CPSS","CPST","CPT","CPTA","CPTAG","CPTAL","CPTI","CQP","CR","CRAI","CRAY","CRBP","CRC","CRCM","CRD.A","CRD.B","CREE","CREG","CRESY","CREX","CREXW","CRF","CRH","CRHM","CRI","CRIS","CRK","CRL","CRM","CRMD","CRMT","CRNT","CRNX","CRON","CROX","CRR","CRS","CRSA","CRSAU","CRSAW","CRSP","CRT","CRTO","CRTX","CRUS","CRVL","CRVS","CRWD","CRWS","CRY","CRZO","CS","CSA","CSB","CSBR","CSCO","CSF","CSFL","CSGP","CSGS","CSII","CSIQ","CSL","CSLT","CSML","CSOD","CSPI","CSQ","CSS","CSSE","CSSEP","CSTE","CSTL","CSTM","CSTR","CSU","CSV","CSWC","CSWCL","CSWI","CSX","CTAA","CTAC","CTACU","CTACW","CTAS","CTA^A","CTA^B","CTB","CTBB","CTBI","CTDD","CTEK","CTEST","CTEST.E","CTEST.G","CTEST.L","CTEST.O","CTEST.S","CTEST.V","CTG","CTHR","CTIB","CTIC","CTK","CTL","CTLT","CTMX","CTO","CTR","CTRA","CTRC","CTRE","CTRM","CTRN","CTRP","CTS","CTSH","CTSO","CTST","CTT","CTV","CTVA","CTWS","CTXR","CTXRW","CTXS","CTY","CTZ","CUB","CUBA","CUBE","CUBI","CUBI^C","CUBI^D","CUBI^E","CUBI^F","CUE","CUI","CUK","CULP","CUO","CUR","CURO","CUTR","CUZ","CVA","CVBF","CVCO","CVCY","CVE","CVEO","CVET","CVGI","CVGW","CVI","CVIA","CVLT","CVLY","CVM","CVNA","CVR","CVRS","CVS","CVTI","CVU","CVV","CVX","CW","CWBC","CWBR","CWCO","CWEN","CWEN.A","CWH","CWK","CWST","CWT","CX","CXDC","CXE","CXH","CXO","CXP","CXSE","CXW","CY","CYAD","CYAN","CYBE","CYBR","CYCC","CYCCP","CYCN","CYD","CYH","CYOU","CYRN","CYRX","CYRXW","CYTK","CZNC","CZR","CZWI","CZZ","C^J","C^K","C^N","C^S","D","DAC","DAIO","DAKT","DAL","DALI","DAN","DAR","DARE","DAVA","DAVE","DAX","DB","DBD","DBI","DBL","DBVT","DBX","DCAR","DCF","DCI","DCIX","DCO","DCOM","DCP","DCPH","DCP^B","DCP^C","DCUE","DD","DDD","DDF","DDIV","DDMX","DDMXU","DDMXW","DDOC","DDS","DDT","DE","DEA","DEAC","DEACU","DEACW","DECK","DEI","DELL","DENN","DEO","DERM","DESP","DEST","DEX","DF","DFBH","DFBHU","DFBHW","DFFN","DFIN","DFNL","DFP","DFRG","DFS","DFVL","DFVS","DG","DGICA","DGICB","DGII","DGLD","DGLY","DGRE","DGRS","DGRW","DGSE","DGX","DHF","DHI","DHIL","DHR","DHR^A","DHT","DHX","DHXM","DHY","DIAX","DIN","DINT","DIOD","DIS","DISCA","DISCB","DISCK","DISH","DIT","DJCO","DK","DKL","DKS","DKT","DL","DLA","DLB","DLBS","DLHC","DLNG","DLNG^A","DLNG^B","DLPH","DLPN","DLPNW","DLR","DLR^C","DLR^G","DLR^I","DLR^J","DLR^K","DLTH","DLTR","DLX","DMAC","DMB","DMF","DMLP","DMO","DMPI","DMRC","DMTK","DMTKW","DNBF","DNI","DNJR","DNKN","DNLI","DNN","DNOW","DNP","DNR","DO","DOC","DOCU","DOGZ","DOMO","DOOO","DOOR","DORM","DOV","DOVA","DOW","DOX","DOYU","DPG","DPHC","DPHCU","DPHCW","DPLO","DPW","DPZ","DQ","DRAD","DRADP","DRD","DRE","DRH","DRI","DRIO","DRIOW","DRIV","DRMT","DRNA","DRQ","DRRX","DRUA","DRYS","DS","DSE","DSGX","DSKE","DSKEW","DSL","DSLV","DSM","DSPG","DSS","DSSI","DSU","DSWL","DSX","DSX^B","DS^B","DS^C","DS^D","DT","DTE","DTEA","DTF","DTIL","DTJ","DTLA^","DTQ","DTSS","DTUL","DTUS","DTV","DTW","DTY","DTYL","DTYS","DUC","DUK","DUKB","DUKH","DUK^A","DUSA","DVA","DVAX","DVD","DVLU","DVN","DVOL","DVY","DWAQ","DWAS","DWAT","DWCR","DWFI","DWIN","DWLD","DWMC","DWPP","DWSH","DWSN","DWTR","DX","DXB","DXC","DXCM","DXF","DXGE","DXJS","DXLG","DXPE","DXR","DXYN","DX^A","DX^B","DY","DYAI","DYNT","DZSI","E","EA","EAB","EAD","EAE","EAF","EAI","EARN","EARS","EAST","EAT","EB","EBAY","EBAYL","EBF","EBIX","EBIZ","EBMT","EBR","EBR.B","EBS","EBSB","EBTC","EC","ECA","ECC","ECCA","ECCB","ECCX","ECCY","ECF","ECF^A","ECHO","ECL","ECOL","ECOM","ECOR","ECOW","ECPG","ECT","ED","EDAP","EDD","EDF","EDI","EDIT","EDN","EDNT","EDRY","EDSA","EDTX","EDTXU","EDTXW","EDU","EDUC","ENTXW","ENV","ENVA","ENX","ENZ","ENZL","EOD","EOG","EOI","EOLS","EOS","EOT","EPAM","EPAY","EPC","EPD","EPIX","EPM","EPR","EPRT","EPR^C","EPR^E","EPR^G","EPSN","EPZM","EP^C","EQ","EQBK","EQC","EQC^D","EQH","EQIX","EQM","EQNR","EQR","EQRR","EQS","EQT","ERA","ERC","ERF","ERH","ERI","ERIC","ERIE","ERII","ERJ","EROS","ERYP","ES","ESBK","ESCA","ESE","ESEA","ESG","ESGD","ESGE","ESGG","ESGR","ESGRO","ESGRP","ESGU","ESI","ESLT","ESNT","ESP","ESPR","ESQ","ESRT","ESS","ESSA","ESTA","ESTC","ESTE","ESTR","ESTRW","ESXB","ET","ETB","ETFC","ETG","ETH","ETI^","ETJ","ETM","ETN","ETO","ETON","ETP^C","ETP^D","ETP^E","ETR","ETRN","ETSY","ETTX","ETV","ETW","ETX","ETY","EUFN","EURN","EV","EVA","EVBG","EVBN","EVC","EVER","EVF","EVFM","EVG","EVGBC","EVGN","EVH","EVI","EVK","EVLMC","EVLO","EVM","EVN","EVOK","EVOL","EVOP","EVR","EVRG","EVRI","EVSI","EVSIW","EVSTC","EVT","EVTC","EVV","EVY","EW","EWBC","EWJE","EWJV","EWZS","EXAS","EXC","EXD","EXEL","EXFO","EXG","EXK","EXLS","EXP","EXPC","EXPCU","EXPD","EXPE","EXPI","EXPO","EXPR","EXR","EXTN","EXTR","EYE","EYEG","EYEGW","EYEN","EYES","EYESW","EYPT","EZPW","EZT","F","FAAR","FAB","FAD","FAF","FALN","FAM","FAMI","FANG","FANH","FARM","FARO","FAST","FAT","FATE","FAX","FB","FBC","FBHS","FBIO","FBIOP","FBIZ","FBK","FBM","FBMS","FBNC","FBP","FBSS","FBZ","FC","FCA","FCAL","FCAN","FCAP","FCAU","FCBC","FCBP","FCCO","FCCY","FCEF","FCEL","FCF","FCFS","FCN","FCNCA","FCO","FCPT","FCSC","FCT","FCVT","FCX","FDBC","FDEF","FDEU","FDIV","FDNI","FDP","FDS","FDT","FDTS","FDUS","FDUSL","FDUSZ","FDX","FE","FEDU","FEI","FEIM","FELE","FELP","FEM","FEMB","FEMS","FEN","FENC","FENG","FEO","FEP","FET","FEUZ","FEX","FEYE","FF","FFA","FFBC","FFBW","FFC","FFG","FFHL","FFIC","FFIN","FFIV","FFNW","FFWM","FG","FG.WS","FGB","FGBI","FGEN","FGM","FGP","FHB","FHK","FHL","FHN","FHN^A","FI","FIBK","FICO","FID","FIF","FII","FINS","FINX","FIS","FISI","FISV","FIT","FITB","FITBI","FITBP","FIV","FIVE","FIVN","FIX","FIXD","FIXX","FIZZ","FJP","FKLY","FKO","FKU","FL","FLAG","FLAT","FLC","FLDM","FLEX","FLGT","FLIC","FLIR","FLL","FLLCU","FLMN","FLMNW","FLN","FLNG","FLNT","FLO","FLOW","FLR","FLS","FLT","FLUX","FLWR","FLWS","FLXN","FLXS","FLY","FMAO","FMAX","FMB","FMBH","FMBI","FMC","FMCI","FMCIU","FMCIW","FMHI","FMK","FMN","FMNB","FMO","FMS","FMX","FMY","FN","FNB","FNB^E","FNCB","FND","FNF","FNHC","FNJN","FNK","FNKO","FNLC","FNSR","FNV","FNWB","FNX","FNY","FOCS","FOE","FOF","FOLD","FOMX","FONR","FOR","FORD","FORK","FORM","FORR","FORTY","FOSL","FOX","FOXA","FOXF","FPA","FPAC","FPAC.U","FPAC.WS","FPAY","FPAYW","FPF","FPH","FPI","FPI^B","FPL","FPRX","FPXE","FPXI","FR","FRA","FRAC","FRAF","FRAN","FRBA","FRBK","FRC","FRC^D","FRC^F","FRC^G","FRC^H","FRC^I","FRD","FRED","FRGI","FRME","FRO","FRPH","FRPT","FRSX","FRT","FRTA","FRT^C","FSB","FSBC","FSBW","FSCT","FSD","FSEA","FSFG","FSI","FSK","FSLR","FSLY","FSM","FSP","FSS","FSTR","FSV","FSZ","FT","FTA","FTAC","FTACU","FTACW","FTAG","FTAI","FTC","FTCH","FTCS","FTDR","FTEK","FTEO","FTF","FTFT","FTGC","FTHI","FTI","FTK","FTLB","FTNT","FTNW","FTR","FTRI","FTS","FTSI","FTSL","FTSM","FTSV","FTV","FTV^A","FTXD","FTXG","FTXH","FTXL","FTXN","FTXO","FTXR","FUL","FULC","FULT","FUN","FUNC","FUND","FUSB","FUV","FV","FVC","FVCB","FVE","FVRR","FWONA","FWONK","FWP","FWRD","FXNC","FYC","FYT","FYX","F^B","G","GAB","GABC","GAB^D","GAB^G","GAB^H","GAB^J","GAIA","GAIN","GAINL","GAINM","GALT","GAM","GAM^B","GARS","GASS","GATX","GBAB","GBCI","GBDC","GBL","GBLI","GBLIL","GBLIZ","GBLK","GBR","GBT","GBX","GCAP","GCBC","GCI","GCO","GCP","GCV","GCVRZ","GCV^B","GD","GDDY","GDEN","GDI","GDL","GDL^C","GDO","GDOT","GDP","GDS","GDV","GDV^A","GDV^D","GDV^G","GDV^H","GE","GEC","GECC","GECCL","GECCM","GECCN","GEF","GEF.B","GEL","GEMP","GEN","GENC","GENE","GENY","GEO","GEOS","GER","GERN","GES","GEVO","GF","GFED","GFF","GFI","GFN","GFNCP","GFNSL","GFY","GGAL","GGB","GGG","GGM","GGN","GGN^B","GGO","GGO^A","GGT","GGT^B","GGT^E","GGZ","GGZ^A","GH","GHC","GHDX","GHG","GHL","GHM","GHSI","GHY","GIB","GIFI","GIG","GIG.U","GIG.WS","GIGE","GIGM","GIG~","GIII","GIL","GILD","GILT","GOLF","GOOD","GOODM","GOODO","GOODP","GOOG","GOOGL","GOOS","GORO","GOSS","GPAQ","GPAQU","GPAQW","GPC","GPI","GPJA","GPK","GPL","GPM","GPMT","GPN","GPOR","GPP","GPRE","GPRK","GPRO","GPS","GPX","GRA","GRAF","GRAF.U","GRAF.WS","GRAM","GRBK","GRC","GRF","GRFS","GRID","GRIF","GRIN","GRMN","GRNQ","GROW","GRP.U","GRPN","GRSH","GRSHU","GRSHW","GRTS","GRUB","GRVY","GRX","GRX^A","GRX^B","GS","GSAH","GSAH.U","GSAH.WS","GSAT","GSB","GSBC","GSBD","GSH","GSHD","GSIT","GSK","GSKY","GSL","GSL^B","GSM","GSS","GSUM","GSV","GSX","GS^A","GS^C","GS^D","GS^J","GS^K","GS^N","GT","GTE","GTES","GTHX","GTIM","GTLS","GTN","GTN.A","GTS","GTT","GTX","GTY","GTYH","GULF","GURE","GUT","GUT^A","GUT^C","GV","GVA","GVP","GWB","GWGH","GWPH","GWR","GWRE","GWRS","GWW","GXGX","GXGXU","GXGXW","GYB","GYC","GYRO","H","HA","HABT","HAE","HAFC","HAIN","HAIR","HAL","HALL","HALO","HARP","HAS","HASI","HAYN","HBAN","HBANN","HBANO","HBB","HBCP","HBI","HBIO","HBM","HBMD","HBNC","HBP","HCA","HCAC","HCACU","HCACW","HCAP","HCAPZ","HCAT","HCC","HCCH","HCCHR","HCCHU","HCCHW","HCCI","HCFT","HCHC","HCI","HCKT","HCM","HCP","HCR","HCSG","HCXY","HCXZ","HD","HDB","HDS","HDSN","HE","HEAR","HEBT","HEES","HEI","HEI.A","HELE","HEP","HEPA","HEQ","HERD","HES","HESM","HEWG","HEXO","HFBL","HFC","HFFG","HFRO","HFRO^A","HFWA","HGH","HGLB","HGSH","HGV","HHC","HHHH","HHHHR","HHHHU","HHHHW","HHR","HHS","HHT","HI","HIBB","HIE","HIFS","HIG","HIG^G","HIHO","HII","HIIQ","HIL","HIMX","HIO","HIW","HIX","HJLI","HJLIW","HJV","HKIB","HL","HLAL","HLF","HLG","HLI","HLIO","HLIT","HLM^","HLNE","HLT","HLX","HL^B","HMC","HMG","HMHC","HMI","HMLP","HMLP^A","HMN","HMNF","HMST","HMSY","HMTV","HMY","HNDL","HNGR","HNI","HNNA","HNP","HNRG","HNW","HOFT","HOG","HOLI","HOLX","HOMB","HOME","HON","HONE","HOOK","HOPE","HOS","HOTH","HOV","HOVNP","HP","HPE","HPF","HPI","HPJ","HPP","HPQ","HPR","HPS","HPT","HQH","HQI","HQL","HQY","HR","HRB","HRC","HRI","HRL","HROW","HRTG","HRTX","HRZN","HSAC","HSACU","HSACW","HSBC","HSBC^A","HSC","HSDT","HSGX","HSIC","HSII","HSKA","HSON","HST","HSTM","HSY","HT","HTA","HTBI","HTBK","HTBX","HTD","HTFA","HTGC","HTGM","HTH","HTHT","HTLD","HTLF","HTY","HTZ","HT^C","HT^D","HT^E","HUBB","HUBG","HUBS","HUD","HUM","HUN","HURC","HURN","HUSA","HUYA","HVBC","HVT","HVT.A","HWBK","HWC","HWCC","HWCPL","HWKN","HX","HXL","HY","HYAC","HYACU","HYACW","HYB","HYI","HYLS","HYND","HYRE","HYT","HYXE","HYZD","HZN","HZNP","HZO","I","IAA","IAC","IAE","IAF","IAG","IART","IBA","IBB","IBCP","IBEX","IBIO","IBKC","IBKCN","IBKCO","IBKCP","IBM","IBN","IBO","IBOC","IBP","IBTX","IBUY","ICAD","ICBK","ICCC","ICCH","ICD","ICE","ICFI","ICHR","ICL","ICLK","ICLN","ICLR","ICMB","ICON","ICPT","ICUI","IDA","IDCC","IDE","IDEX","IDLB","IDN","IDRA","IDSA","INO","INOD","INOV","INPX","INS","INSE","INSG","INSI","INSM","INSP","INST","INSU","INSUU","INSUW","INSW","INSW^A","INT","INTC","INTG","INTL","INTT","INTU","INUV","INVA","INVE","INVH","INWK","INXN","IO","IONS","IOR","IOSP","IOTS","IOVA","IP","IPAR","IPB","IPDN","IPG","IPGP","IPHI","IPHS","IPI","IPKW","IPLDP","IPOA","IPOA.U","IPOA.WS","IPWR","IQ","IQI","IQV","IR","IRBT","IRCP","IRDM","IRET","IRET^C","IRIX","IRL","IRM","IRMD","IROQ","IRR","IRS","IRT","IRTC","IRWD","ISBC","ISCA","ISD","ISDR","ISDS","ISDX","ISEE","ISEM","ISG","ISHG","ISIG","ISNS","ISR","ISRG","ISRL","ISSC","ISTB","ISTR","IT","ITCB","ITCI","ITEQ","ITGR","ITI","ITIC","ITMR","ITP","ITRI","ITRM","ITRN","ITT","ITUB","ITW","IUS","IUSB","IUSG","IUSS","IUSV","IVAC","IVC","IVH","IVR","IVR^A","IVR^B","IVR^C","IVZ","IX","IXUS","IZEA","JACK","JAG","JAGX","JAKK","JAN","JASN","JAX","JAZZ","JBGS","JBHT","JBK","JBL","JBLU","JBN","JBR","JBSS","JBT","JCAP","JCAP^B","JCE","JCI","JCO","JCOM","JCP","JCS","JCTCF","JD","JDD","JE","JEC","JEF","JELD","JEMD","JEQ","JE^A","JFIN","JFK","JFKKR","JFKKU","JFKKW","JFR","JFU","JG","JGH","JHAA","JHB","JHD","JHG","JHI","JHS","JHX","JHY","JILL","JJSF","JKHY","JKI","JKS","JLL","JLS","JMEI","JMF","JMIA","JMLP","JMM","JMP","JMPB","JMPD","JMT","JMU","JNCE","JNJ","JNPR","JOB","JOBS","JOE","JOF","JOUT","JP","JPC","JPI","JPM","JPM^A","JPM^C","JPM^D","JPM^F","JPM^G","JPM^H","JPS","JPT","JQC","JRI","JRJC","JRO","JRS","JRSH","JRVR","JSD","JSM","JSMD","JSML","JT","JTA","JTD","JVA","JW.A","JW.B","JWN","JYNT","K","KAI","KALA","KALU","KALV","KAMN","KAR","KB","KBAL","KBH","KBLM","KBLMR","KBLMU","KBLMW","KBR","KBSF","KBWB","KBWD","KBWP","KBWR","KBWY","KCAPL","KDMN","KDP","KE","KEG","KELYA","KELYB","KEM","KEN","KEP","KEQU","KERN","KERNW","KEX","KEY","KEYS","KEY^I","KEY^J","KEY^K","KF","KFFB","KFRC","KFS","KFY","KGC","KGJI","KHC","KIDS","KIM","KIM^I.CL","KIM^J","KIM^K.CL","KIM^L","KIM^M","KIN","KINS","KIO","KIQ","KIRK","KKR","KKR^A","KKR^B","KL","KLAC","KLDO","KLIC","KLXE","KMB","KMDA","KMF","KMI","KMPH","KMPR","KMT","KMX","KN","KNDI","KNL","KNOP","KNSA","KNSL","KNX","KO","KOD","KODK","KOF","KOOL","KOP","KOPN","KOS","KOSS","KPTI","KR","KRA","KRC","KREF","KRG","KRMA","KRNT","KRNY","KRO","KRP","KRTX","KRUS","KRYS","KSM","KSS","KSU","KSU^","KT","KTB","KTCC","KTF","KTH","KTN","KTOS","KTOV","KTOVW","KTP","KURA","KVHI","KW","KWEB","KWR","KXIN","KYN","KYN^F","KZIA","KZR","L","LAC","LACQ","LACQU","LACQW","LAD","LADR","LAIX","LAKE","LAMR","LANC","LAND","LANDP","LARK","LASR","LAUR","LAWS","LAZ","LAZY","LB","LBAI","LBC","LBRDA","LBRDK","LBRT","LBTYA","LBTYB","LBTYK","LBY","LBYAV","LBYKV","LC","LCA","LCAHU","LCAHW","LCI","LCII","LCNB","LCTX","LCUT","LDL","LDOS","LDP","LDRI","LDSF","LOMA","LONE","LOOP","LOPE","LOR","LORL","LOV","LOVE","LOW","LPCN","LPG","LPI","LPL","LPLA","LPSN","LPT","LPTH","LPTX","LPX","LQDA","LQDT","LRAD","LRCX","LRGE","LRN","LSBK","LSCC","LSI","LSTR","LSXMA","LSXMB","LSXMK","LTBR","LTC","LTHM","LTM","LTRPA","LTRPB","LTRX","LTS","LTSF","LTSH","LTSK","LTSL","LTS^A","LTXB","LUB","LULU","LUNA","LUNG","LUV","LVGO","LVHD","LVS","LW","LWAY","LX","LXFR","LXP","LXP^C","LXRX","LXU","LYB","LYFT","LYG","LYL","LYTS","LYV","LZB","M","MA","MAA","MAA^I","MAC","MACK","MAG","MAGS","MAIN","MAMS","MAN","MANH","MANT","MANU","MAR","MARA","MARK","MARPS","MAS","MASI","MAT","MATW","MATX","MAV","MAXR","MAYS","MBB","MBCN","MBI","MBII","MBIN","MBINO","MBINP","MBIO","MBOT","MBRX","MBSD","MBT","MBUU","MBWM","MC","MCA","MCB","MCBC","MCC","MCD","MCEF","MCEP","MCF","MCFT","MCHI","MCHP","MCHX","MCI","MCK","MCN","MCO","MCR","MCRB","MCRI","MCRN","MCS","MCV","MCX","MCY","MD","MDB","MDC","MDCA","MDCO","MDGL","MDGS","MDGSW","MDIV","MDJH","MDLA","MDLQ","MDLX","MDLY","MDLZ","MDP","MDR","MDRR","MDRX","MDSO","MDT","MDU","MDWD","MEC","MED","MEDP","MEET","MEI","MEIP","MELI","MEN","MEOH","MERC","MER^K","MESA","MESO","MET","METC","MET^A","MET^E","MFA","MFAC","MFAC.U","MFAC.WS","MFA^B","MFC","MFD","MFG","MFGP","MFIN","MFINL","MFL","MFM","MFNC","MFO","MFSF","MFT","MFV","MG","MGA","MGEE","MGEN","MGF","MGI","MGIC","MGLN","MGM","MGNX","MGP","MGPI","MGR","MGRC","MGTA","MGTX","MGU","MGY","MGYR","MHD","MHE","MHF","MHH","MHI","MHK","MHLA","MHLD","MHN","MHNC","MHO","MH^A","MH^C","MH^D","MIC","MICR","MICT","MIDD","MIE","MIK","MILN","MIME","MIN","MIND","MINDP","MINI","MIRM","MIST","MITK","MITO","MITT","MITT^A","MITT^B","MIXT","MIY","MJCO","MKC","MKC.V","MKGI","MKL","MKSI","MKTX","MLAB","MLCO","MLHR","MLI","MLM","MLND","MLNT","MLNX","MLP","MLR","MLSS","MLVF","MMAC","MMC","MMD","MMI","MMLP","MMM","MMP","MMS","MMSI","MMT","MMU","MMX","MMYT","MN","MNCL","MNCLU","MNCLW","MNDO","MNE","MNI","MNK","MNKD","MNLO","MNOV","MNP","MNR","MNRL","MNRO","MNR^C","MNSB","MNST","MNTA","MNTX","MO","MOBL","NCMI","NCNA","NCR","NCSM","NCTY","NCV","NCV^A","NCZ","NCZ^A","NDAQ","NDLS","NDP","NDRA","NDRAW","NDSN","NE","NEA","NEBU","NEBUU","NEBUW","NEE","NEE^I","NEE^J","NEE^K","NEE^N","NEE^O","NEM","NEN","NEO","NEOG","NEON","NEOS","NEP","NEPH","NEPT","NERV","NES","NESR","NESRW","NETE","NEU","NEV","NEW","NEWA","NEWM","NEWR","NEWT","NEWTI","NEWTL","NEXA","NEXT","NFBK","NFC","NFC.U","NFC.WS","NFE","NFG","NFIN","NFINU","NFINW","NFJ","NFLX","NFTY","NG","NGD","NGG","NGHC","NGHCN","NGHCO","NGHCP","NGHCZ","NGL","NGLS^A","NGL^B","NGL^C","NGM","NGS","NGVC","NGVT","NH","NHA","NHC","NHF","NHI","NHLD","NHLDW","NHS","NHTC","NI","NICE","NICK","NID","NIE","NIHD","NIM","NINE","NIO","NIQ","NIU","NI^B","NJR","NJV","NK","NKE","NKG","NKSH","NKTR","NKX","NL","NLNK","NLS","NLSN","NLTX","NLY","NLY^D","NLY^F","NLY^G","NLY^I","NM","NMCI","NMFC","NMFX","NMI","NMIH","NMK^B","NMK^C","NML","NMM","NMR","NMRD","NMRK","NMS","NMT","NMY","NMZ","NM^G","NM^H","NNA","NNBR","NNC","NNDM","NNI","NNN","NNN^E.CL","NNN^F","NNVC","NNY","NOA","NOAH","NOC","NODK","NOG","NOK","NOM","NOMD","NOV","NOVA","NOVN","NOVT","NOW","NP","NPAUU","NPK","NPN","NPO","NPTN","NPV","NQP","NR","NRC","NRCG","NRCG.WS","NRE","NRG","NRGX","NRIM","NRK","NRO","NRP","NRT","NRUC","NRZ","NRZ^A","NRZ^B","NS","NSA","NSA^A","NSC","NSCO","NSCO.WS","NSEC","NSIT","NSL","NSP","NSPR","NSPR.WS","NSPR.WS.B","NSS","NSSC","NSTG","NSYS","NS^A","NS^B","NS^C","NTAP","NTB","NTC","NTCT","NTEC","NTES","NTEST","NTEST.A","NTEST.B","NTEST.C","NTG","NTGN","NTGR","NTIC","NTIP","NTLA","NTN","NTNX","NTP","NTR","NTRA","NTRP","NTRS","NTRSP","NTUS","NTWK","NTX","NTZ","NUAN","NUE","NUM","NUO","NURO","NUROW","NUS","NUV","NUVA","NUW","NVAX","NVCN","NVCR","NVDA","NVEC","NVEE","NVFY","NVG","NVGS","NVIV","NVLN","NVMI","NVO","NVR","NVRO","NVS","NVT","NVTA","NVTR","NVUS","NWBI","NWE","NWFL","NWHM","NWL","NWLI","NWN","NWPX","NWS","NWSA","NX","NXC","NXE","NXGN","NXJ","NXMD","NXN","NXP","NXPI","NXQ","NXR","NXRT","NXST","NXTC","NXTD","NXTG","NYCB","NYCB^A","NYCB^U","NYMT","NYMTN","NYMTO","NYMTP","NYMX","NYNY","NYT","NYV","NZF","O","OAC","OAC.U","OAC.WS","OAK","OAK^A","OAK^B","OAS","OBAS","OBCI","OBE","OBLN","OBNK","OBSV","OC","OCC","OCCI","OCCIP","OCFC","OCN","OCSI","OCSL","OCSLL","OCUL","OCX","ODC","ODFL","ODP","ODT","OEC","OESX","OFC","OFED","OFG","OFG^A","OFG^B","OFG^D","OFIX","OFLX","OFS","OFSSL","OFSSZ","OGE","OGEN","OGI","OGS","OHAI","OHI","OI","OIA","OIBR.C","OII","OIIM","OIS","OKE","OKTA","OLBK","OLD","OLED","OLLI","OLN","OLP","OMAB","OMC","OMCL","OMER","OMEX","OMF","OMI","OMN","OMP","ON","ONB","ONCE","ONCS","ONCT","ONCY","ONDK","ONE","ONEQ","ONTX","ONTXW","ONVO","OOMA","OPB","OPBK","OPES","OPESU","OPESW","OPGN","OPGNW","OPHC","OPI","OPINI","OPK","OPNT","OPOF","OPP","OPRA","OPRX","OPTN","OPTT","OPY","OR","ORA","ORAN","ORBC","ORC","ORCC","ORCL","ORG","ORGO","ORGS","ORI","ORIT","ORLY","ORMP","ORN","ORRF","ORSNU","ORTX","OSB","OSBC","OSBCP","OSG","OSIS","OSK","OSLE","OSMT","OSN","OSPN","OSS","OSTK","OSUR","OSW","OTEL","OTEX","OTG","OTIC","OTIV","OTLK","OTLKW","OTTR","OTTW","OUT","OVBC","OVID","OVLY","OXBR","OXBRW","OXFD","OXLC","OXLCM","OXLCO","OXM","OXSQ","OXSQL","OXSQZ","OXY","OZK","PAA","PAAC","PAACR","PAACU","PAACW","PAAS","PAC","PACB","PACD","PACK","PACK.WS","PACQ","PACQU","PACQW","PACW","PAG","PAGP","PAGS","PAHC","PAI","PAM","PANL","PANW","PAR","PARR","PATI","PATK","PAVM","PAVMW","PAVMZ","PAYC","PAYS","PAYX","PB","PBA","PBB","PBBI","PBC","PBCT","PBCTP","PBF","PBFS","PBFX","PBH","PBHC","PBI","PBIO","PBIP","PBI^B","PBPB","PBR","PBR.A","PBT","PBTS","PBY","PBYI","PCAR","PCB","PCF","PCG","PCG^A","PCG^B","PCG^C","PCG^D","PCG^E","PCG^G","PCG^H","PCG^I","PCH","PCI","PCIM","PCK","PCM","PCN","PCOM","PCQ","PCRX","PCSB","PCTI","PCTY","PCYG","PCYO","PD","PDBC","PDCE","PDCO","PDD","PDEV","PDEX","PDFS","PDI","PDLB","PDLI","PDM","PDP","PDS","PDSB","PDT","PE","PEB","PEBK","PEBO","PEB^C","PEB^D","PEB^E","PEB^F","PECK","PED","PEER","PEG","PEGA","PEGI","PEI","PEIX","PEI^B","PEI^C","PEI^D","PEN","PENN","PEO","PEP","PER","PERI","PESI","PETQ","PETS","PETZ","PEY","PEZ","PFBC","PFBI","PFD","PFE","PFF","PFG","PFGC","PFH","PFI","PFIE","PFIN","PME","PMF","PML","PMM","PMO","PMOM","PMT","PMTS","PMT^A","PMT^B","PMX","PNBK","PNC","PNC^P","PNC^Q","PNF","PNFP","PNI","PNM","PNNT","PNQI","PNR","PNRG","PNRL","PNTR","PNW","POAI","PODD","POL","POLA","POLY","POOL","POPE","POR","POST","POWI","POWL","PPBI","PPC","PPDF","PPG","PPH","PPHI","PPIH","PPL","PPR","PPSI","PPT","PPX","PQG","PRA","PRAA","PRAH","PRCP","PRE^F","PRE^G","PRE^H","PRE^I","PRFT","PRFZ","PRGO","PRGS","PRGX","PRH","PRI","PRIF^A","PRIF^B","PRIF^C","PRIF^D","PRIM","PRK","PRLB","PRMW","PRN","PRNB","PRO","PROS","PROV","PRPH","PRPL","PRPO","PRQR","PRS","PRSC","PRSP","PRT","PRTA","PRTH","PRTK","PRTO","PRTS","PRTY","PRU","PRVB","PRVL","PS","PSA","PSA^A","PSA^B","PSA^C","PSA^D","PSA^E","PSA^F","PSA^G","PSA^H","PSA^V","PSA^W","PSA^X","PSB","PSB^U","PSB^V","PSB^W","PSB^X","PSB^Y","PSC","PSCC","PSCD","PSCE","PSCF","PSCH","PSCI","PSCM","PSCT","PSCU","PSDO","PSEC","PSET","PSF","PSL","PSM","PSMT","PSN","PSNL","PSO","PSTG","PSTI","PSTL","PSTV","PSTVZ","PSV","PSX","PSXP","PT","PTC","PTCT","PTE","PTEN","PTF","PTGX","PTH","PTI","PTLA","PTMN","PTN","PTNR","PTR","PTSI","PTVCA","PTVCB","PTY","PUB","PUI","PUK","PUK^","PUK^A","PULM","PUMP","PUYI","PVAC","PVAL","PVBC","PVG","PVH","PVL","PVT","PVT.U","PVT.WS","PVTL","PW","PWOD","PWR","PW^A","PXD","PXI","PXLW","PXS","PY","PYN","PYPL","PYS","PYT","PYX","PYZ","PZC","PZG","PZN","PZZA","QABA","QADA","QADB","REG","REGI","REGN","REI","REKR","RELL","RELV","RELX","RENN","REPH","REPL","RES","RESI","RESN","RETA","RETO","REV","REVG","REX","REXN","REXR","REXR^A","REXR^B","REZI","RF","RFAP","RFDI","RFEM","RFEU","RFI","RFIL","RFL","RFP","RF^A","RF^B","RF^C","RGA","RGCO","RGEN","RGLD","RGLS","RGNX","RGR","RGS","RGT","RH","RHE","RHE^A","RHI","RHP","RIBT","RICK","RIF","RIG","RIGL","RILY","RILYG","RILYH","RILYI","RILYL","RILYO","RILYZ","RING","RIO","RIOT","RIV","RIVE","RJF","RKDA","RL","RLGT","RLGY","RLH","RLI","RLJ","RLJ^A","RM","RMAX","RMBI","RMBL","RMBS","RMCF","RMD","RMED","RMG","RMG.U","RMG.WS","RMI","RMM","RMNI","RMPL^","RMR","RMT","RMTI","RNDB","RNDM","RNDV","RNEM","RNET","RNG","SEAC","SEAS","SEB","SECO","SEDG","SEE","SEED","SEEL","SEIC","SELB","SELF","SEM","SEMG","SENEA","SENEB","SENS","SERV","SES","SESN","SF","SFB","SFBC","SFBS","SFE","SFET","SFIX","SFL","SFLY","SFM","SFNC","SFST","SFUN","SF^A","SF^B","SG","SGA","SGB","SGBX","SGC","SGEN","SGH","SGLB","SGLBW","SGMA","SGMO","SGMS","SGOC","SGRP","SGRY","SGU","SHAK","SHBI","SHEN","SHG","SHI","SHIP","SHIPW","SHIPZ","SHLL","SHLL.U","SHLL.WS","SHLO","SHLX","SHO","SHOO","SHOP","SHOS","SHO^E","SHO^F","SHSP","SHV","SHW","SHY","SIBN","SIC","SID","SIEB","SIEN","SIF","SIFY","SIG","SIGA","SIGI","SILC","SILK","SILV","SIM","SIMO","SINA","SINO","SINT","SIRI","SITC","SITC^A","SITC^J","SITC^K","SITE","SITO","SIVB","SIX","SJI","SJIU","SJM","SJR","SJT","SJW","SKIS","SKM","SKOR","SKT","SKX","SKY","SKYS","SKYW","SKYY","SLAB","SLB","SLCA","SLCT","SLDB","SLF","SLG","SLGG","SLGL","SLGN","SLG^I","SLIM","SLM","SLMBP","SLNG","SLNO","SLNOW","SLP","SLQD","SLRC","SLRX","SLS","SLVO","SM","SMAR","SMBC","SMBK","SMCP","SMED","SMFG","SMG","SMHI","SMIT","SMLP","SMM","SMMC","SMMCU","SMMCW","SMMF","SMMT","SMP","SMPL","SMRT","SMSI","SMTA","SMTC","SMTS","SMTX","SNA","SNAP","SNBR","SNCR","SND","SNDE","SNDL","SNDR","SNDX","SNE","SNES","SNFCA","SNGX","SNGXW","SNH","SNHNI","SNHNL","SNLN","SNMP","SNN","SNNA","SNOA","SNOAW","SNP","SNPS","SNR","SNSR","SNSS","SNV","SNV^D","SNV^E","SNX","SNY","SO","SOCL","SOGO","SOHO","SOHOB","SOHON","SOHOO","SOHU","SOI","SOJA","SOJB","SOJC","SOL","SOLN","SOLO","SOLOW","SOLY","SON","SONA","SONG","SONGW","SONM","SONO","SOR","SORL","SOXX","SP","SPAQ","SPAQ.U","SPAQ.WS","SPAR","SPB","SPCB","SPE","SPEX","SPE^B","SPFI","SPG","SPGI","SPG^J","SPH","SPHS","SPI","SPKE","SPKEP","SPLK","SPLP","SPLP^A","SPN","SPNE","SPNS","SPOK","SPOT","SPPI","SPR","SPRO","SPRT","SPSC","SPTN","SPWH","SPWR","SPXC","SPXX","SQ","SQBG","SQLV","SQM","SQNS","SQQQ","SR","SRAX","SRC","SRCE","SRCI","SRCL","SRC^A","SRDX","SRE","SREA","SRET","SREV","SRE^A","SRE^B","SRF","SRG","SRG^A","SRI","SRL","SRLP","SRNE","SRPT","SRRA","SRRK","SRT","SRTS","SRTSW","SRV","SRVA","SR^A","SSB","SSBI","SSD","SSFN","SSI","SSKN","SSL","SSNC","SSNT","SSP","SSPKU","SSRM","SSSS","SSTI","SSTK","SSW","SSWA","SSW^D","SSW^E","SSW^G","SSW^H","SSW^I","SSY","SSYS","ST","STAA","STAF","STAG","STAG^C","STAR","STAR^D","STAR^G","STAR^I","STAY","STBA","STC","STCN","STE","STFC","STG","STI","STIM","STI^A","STK","STKL","STKS","STL","STLD","STL^A","STM","STML","STMP","STN","STND","STNE","STNG","STNL","STNLU","STNLW","STOK","STON","STOR","STPP","STRA","STRL","STRM","STRO","STRS","STRT","STT","STT^C","STT^D","STT^E","STT^G","STWD","STX","STXB","STXS","STZ","STZ.B","SU","SUI","SUM","SUMR","SUN","SUNS","SUNW","SUP","SUPN","SUPV","SURF","SUSB","SUSC","SUSL","SUZ","SVA","SVBI","SVM","SVMK","SVRA","SVT","SVVC","SWAV","SWCH","SWI","SWIR","SWJ","SWK","SWKS","SWM","SWN","SWP","SWTX","SWX","SWZ","SXC","SXI","SXT","SXTC","SY","SYBT","SYBX","SYF","SYK","SYKE","SYMC","SYN","SYNA","SYNC","SYNH","SYNL","SYPR","SYRS","SYX","SYY","SZC","T","TA","TAC","TACO","TACOW","TACT","TAIT","TAK","TAL","TALO","TALO.WS","TANH","TANNI","TANNL","TANNZ","TAOP","TAP","TAP.A","TAPR","TARO","TAST","TAT","TATT","TAYD","TBB","TBBK","TBC","TBI","TBIO","TBK","TBLT","TBLTU","TBLTW","TBNK","TBPH","TC","TCBI","TCBIL","TCBIP","TCBK","TCCO","TCDA","TCF","TCFC","TCFCP","TCGP","TCI","TCMD","TCO","TCON","TCO^J","TCO^K","TCP","TCPC","TCRD","TCRR","TCRW","TCRZ","TCS","TCX","TD","TDA","TDAC","TDACU","TDACW","TDC","TDE","TDF","TDG","TDI","TDIV","TDJ","TDOC","TDS","TDW","TDW.WS","TDW.WS.A","TDW.WS.B","TDY","TEAF","TEAM","TECD","TECH","TECK","TECTP","TEDU","TEF","TEI","TEL","TELL","TEN","TENB","TENX","TEO","TER","TERP","TESS","TEUM","TEVA","TEX","TFSL","TFX","TG","TGA","TGB","TGC","TGE","TGEN","TGH","TGI","TGLS","TGNA","TGP","TGP^A","TGP^B","TGS","TGT","TGTX","TH","THBRU","THC","THCA","THCAU","THCAW","THCB","THCBU","THCBW","THFF","THG","THGA","THM","THO","THOR","THQ","THR","THRM","THS","THW","THWWW","TIBR","TIBRU","TIBRW","TIF","TIGO","TIGR","TILE","TIPT","TISI","TITN","TIVO","TJX","TK","TKAT","TKC","TKKS","TKKSR","TKKSU","TKKSW","TKR","TLC","TLF","TLGT","TLI","TLK","TLND","TLRA","TLRD","TLRY","TLSA","TLT","TLYS","TM","TMCX","TMCXU","TMCXW","TMDI","TMDX","TME","TMHC","TMO","TMP","TMQ","TMSR","TMST","TMUS","TNAV","TNC","TNDM","TNET","TNK","TNP","TNP^C","TNP^D","TNP^E","TNP^F","TNXP","TOCA","TOL","TOO","TOO^A","TOO^B","TOO^E","TOPS","TORC","TOT","TOTA","TOTAR","TOTAU","TOTAW","TOUR","TOWN","TOWR","TPB","TPC","TPCO","TPGH","TPGH.U","TPGH.WS","TPH","TPHS","TPIC","TPL","TPR","TPRE","TPTX","TPVG","TPVY","TPX","TPZ","TQQQ","TR","TRC","TRCB","TRCH","TRCO","TREC","TREE","TREX","TRGP","TRHC","TRI","TRIB","TRIL","TRIP","TRK","TRMB","TRMD","TRMK","TRMT","TRN","TRNE","TRNE.U","TRNE.WS","TRNO","TRNS","TRNX","TROV","TROW","TROX","TRP","TRPX","TRQ","TRS","TRST","TRT","TRTN","TRTN^A","TRTN^B","TRTX","TRU","TRUE","TRUP","TRV","TRVG","TRVI","TRVN","TRWH","TRX","TRXC","TS","TSBK","TSC","TSCAP","TSCBP","TSCO","TSE","TSEM","TSG","TSI","TSLA","TSLF","TSLX","TSM","TSN","TSQ","TSRI","TSS","TSU","TTC","TTD","TTEC","TTEK","TTGT","TTI","TTM","TTMI","TTNP","TTOO","TTP","TTPH","TTS","TTTN","TTWO","TU","TUES","TUFN","TUP","TUR","TURN","TUSA","TUSK","TV","TVC","TVE","TVIX","TVTY","TW","TWI","TWIN","TWLO","TWMC","TWN","TWNK","TWNKW","TWO","TWOU","TWO^A","TWO^B","TWO^C","TWO^D","TWO^E","TWST","TWTR","TX","TXG","TXMD","TXN","TXRH","TXT","TY","TYG","TYHT","TYL","TYME","TYPE","TY^","TZAC","TZACU","TZACW","TZOO","UA","UAA","UAE","UAL","UAMY","UAN","UAVS","UBA","UBCP","USWSW","USX","UTF","UTG","UTHR","UTI","UTL","UTMD","UTSI","UTX","UUU","UUUU","UUUU.WS","UVE","UVSP","UVV","UXIN","UZA","UZB","UZC","V","VAC","VAL","VALE","VALU","VALX","VAM","VAPO","VAR","VBF","VBFC","VBIV","VBLT","VBND","VBTX","VC","VCEL","VCF","VCIF","VCIT","VCLT","VCNX","VCRA","VCSH","VCTR","VCV","VCYT","VEC","VECO","VEDL","VEEV","VEON","VER","VERB","VERBW","VERI","VERU","VERY","VER^F","VET","VETS","VFC","VFF","VFL","VG","VGI","VGIT","VGLT","VGM","VGR","VGSH","VGZ","VHC","VHI","VIA","VIAB","VIAV","VICI","VICR","VIDI","VIGI","VIIX","VIOT","VIPS","VIRC","VIRT","VISL","VIST","VISTER","VIV","VIVE","VIVO","VJET","VKI","VKQ","VKTX","VKTXW","VLGEA","VLO","VLRS","VLRX","VLT","VLY","VLYPO","VLYPP","VMBS","VMC","VMD","VMET","VMI","VMM","VMO","VMW","VNCE","VNDA","VNE","VNET","VNO","VNOM","VNO^K","VNO^L","VNO^M","VNQI","VNRX","VNTR","VOC","VOD","VOLT","VONE","VONG","VONV","VOXX","VOYA","VOYA^B","VPG","VPV","VRA","VRAY","VRCA","VREX","VRIG","VRML","VRNA","VRNS","VRNT","VRRM","VRS","VRSK","VRSN","VRTS","VRTSP","VRTU","VRTV","VRTX","VSAT","VSDA","VSEC","VSH","VSI","VSLR","VSM","VSMV","VST","VST.WS.A","VSTM","VSTO","VTA","VTC","VTEC","VTGN","VTHR","VTIP","VTIQ","VTIQU","VTIQW","VTN","VTNR","VTR","VTSI","VTUS","VTVT","VTWG","VTWO","VTWV","VUSE","VUZI","VVI","VVPR","VVR","VVUS","VVV","VWOB","VXRT","VXUS","VYGR","VYMI","VZ","W","WAAS","WAB","WABC","WAFD","WAFU","WAIR","WAL","WALA","WASH","WAT","WATT","WB","WBA","WBAI","WBC","WBK","WBND","WBS","WBS^F","WBT","WCC","WCG","WCLD","WCN","WD","WDAY","WDC","WDFC","WDR","WEA","WEBK","WEC","WEI","WELL","WEN","WERN","WES","WETF","WEX","WEYS","WF","WFC","WFC^L","WFC^N","WFC^O","WFC^P","WFC^Q","WFC^R","WFC^T","WFC^V","WFC^W","WFC^X","WFC^Y","WFE^A","WGO","WH","WHD","WHF","WHFBZ","WHG","WHLM","WHLR","WHLRD","WHLRP","WHR","WIA","WIFI","WILC","WINA","WINC","WING","WINS","WIRE","WISA","WIT","WIW","WIX","WK","WKHS","WLDN","WLFC","WLH","WLK","WLKP","WLL","WLTW","WM","WMB","WMC","WMGI","WMK","WMS","WMT","WNC","WNEB","WNFM","WNS","WOOD","WOR","WORK","WORX","WOW","WPC","WPG","WPG^H","WPG^I","WPM","WPP","WPRT","WPX","WRB","WRB^B","WRB^C","WRB^D","WRB^E","WRE","WRI","WRK","WRLD","WRLS","WRLSR","WRLSU","WRLSW","WRN","WRTC","WSBC","WSBF","WSC","WSFS","WSG","WSM","WSO","WSO.B","WSR","WST","WSTG","WSTL","WTBA","WTER","WTFC","WTFCM","WTI","WTM","WTR","WTRE","WTREP","WTRH","WTRU","WTS","WTT","WTTR","WU","WUBA","WVE","WVFC","WVVI","WVVIP","WW","WWD","WWE","WWR","WWW","WY","WYND","WYNN","WYY","X","XAIR","XAN","XAN^C","XBIO","XBIOW","XBIT","XCUR","XEC","XEL","XELA","XELB","XENE","XENT","XERS","XFLT","XFOR","XHR","XIN","XLNX","XLRN","XNCR","XNET","XOG","XOM","XOMA","XON","XONE","XPEL","XPER","XPL","XPO","XRAY","XRF","XRX","XSPA","XT","XTLB","XTNT","XXII","XYF","XYL","Y","YCBD","YELP","YETI","YEXT","YGYI","YI","YIN","YJ","YLCO","YLDE","YMAB","YNDX","YORW","YPF","YRCW","YRD","YTEN","YTRA","YUM","YUMA","YUMC","YVR","YY","Z","ZAGG","ZAYO","ZBH","ZBIO","ZBK","ZBRA","ZB^A","ZB^G","ZB^H","ZDGE","ZEAL","ZEN","ZEUS","ZF","ZFGN","ZG","ZGNX","ZION","ZIONW","ZIOP","ZIV","ZIXI","ZKIN","ZLAB","ZM","ZN","ZNGA","ZNH","ZNWAA","ZOM","ZS","ZSAN","ZTEST","ZTO","ZTR","ZTS","ZUMZ","ZUO","ZVO","ZYME","ZYNE","ZYXI"];

$('#ticker-search, #modalTicker1, #modalTicker3').autocomplete({
	source: function (request, response) {
		var matches = $.map(ticker_dic, function (acItem) {
			if (acItem.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
				return acItem;
			}
		});
		response(matches);
	},
	select: function( event, ui ) {
		let ticker = ui.item.value;
		showChartFromPortfolio(ticker);
	}
});


$( document ).ready(function(){
	$('.mdb-select').materialSelect();
})

// Right Col, Trade Manager
$(document).ready(function () {

	var table = $('#contentEx').DataTable({
		processing: true,
		ajax: {
			"url": "/tradeapi",
			"dataSrc" : function(jsonObj) {
				return jsonObj["_embedded"]["trade"];
			}
		},
		order: [[ 0, "desc" ]],
		columns: [{
			title: "Time",
			data: "date"
		},
			{
				title: "Ticker",
				data: "ticker"
			},
			{
				title: "Quantity",
				data: "quantity"
			},
			{
				title: "Price",
				data: "price"
			},
			{
				title: "Status",
				data: "status"
			}
		]
	});

	setInterval(function(){table.ajax.reload()}, 3000);


});


// Trade, Buy and Sell

$(document).ready(function () {
	$("#transmitBuy").click(function(){
		let data = {
			ticker: $("#modalTicker1").val().trim().toUpperCase(),
			price: parseFloat(latest_price),
			quantity: parseInt($("#modalTicker2").val())
		};
		$.ajax({
			url: '/tradeapi',
			type: "POST",
			data: JSON.stringify(data),
			contentType: "application/json",
			complete: function(event){
				$('#modalLRForm').modal('toggle');
			}
		});
	});

	$("#transmitSell").click(function(){
		let data = {
			ticker: $("#modalTicker3").val().trim().toUpperCase(),
			price: parseFloat(latest_price),
			quantity: 0 - parseInt($("#modalTicker4").val())
		};
		$.ajax({
			url: '/tradeapi',
			type: "POST",
			data: JSON.stringify(data),
			contentType: "application/json",
			complete: function(event){
				$('#modalLRForm').modal('toggle');
			}
		});
	});
});
