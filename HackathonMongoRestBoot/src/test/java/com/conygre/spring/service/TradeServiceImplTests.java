package com.conygre.spring.service;

import com.conygre.spring.data.TradeRepository;
import com.conygre.spring.entities.Trade;
import com.conygre.spring.entities.TradeState;
import org.bson.types.ObjectId;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

public class TradeServiceImplTests {

    private final TradeRepository repo = mock(TradeRepository.class);

    @Test
    public void shouldInsertTradeUsingRepoDao() {
        TradeService service = new TradeServiceImpl(repo);
        Trade trade = new Trade("", "", 0, 0, TradeState.REJECTED);
        service.addTrade(trade);

        verify(repo).insert(trade);
    }

    @Test
    public void shouldReturnUpdatedTickerWhenTradeIfStatusIsCreated() {
        TradeService service = new TradeServiceImpl(repo);

        Trade trade = new Trade("", "UPDATED", 1, 2, TradeState.FILLED);
        Trade oldTrade = new Trade("", "OLD", 0, 0, TradeState.CREATED);
        ObjectId id = ObjectId.get();
        oldTrade.setId(id);
        trade.setId(id);

        when(repo.findById(any(ObjectId.class))).thenReturn(Optional.of(oldTrade));
        when(repo.save(trade)).thenReturn(trade);

        Trade nonUpdated = service.updateTrade(trade);

        assertThat(nonUpdated.getTicker(), Matchers.is("UPDATED"));
        assertThat(nonUpdated.getStatus(), Matchers.is(TradeState.FILLED));
        assertThat(nonUpdated.getQuantity(), Matchers.is(1));
        assertThat(nonUpdated.getPrice(), Matchers.is(2));
    }

    @Test
    public void shouldNotReturnUpdatedTickerWhenTradeIfStatusIsntCreated() {
        TradeRepository filledRepo = mock(TradeRepository.class);
        TradeService service = new TradeServiceImpl(filledRepo);

        Trade trade = new Trade("", "UPDATED", 0, 0, TradeState.FILLED);
        Trade oldTrade = new Trade("", "OLD", 0, 0, TradeState.FILLED);
        ObjectId id = ObjectId.get();
        oldTrade.setId(id);
        trade.setId(id);

        when(filledRepo.findById(any(ObjectId.class))).thenReturn(Optional.of(oldTrade));
        when(filledRepo.save(trade)).thenReturn(trade);


        assertThat(service.updateTrade(trade), Matchers.nullValue());
    }

    @Test
    public void shouldDeleteTradeUsingRepoDao() {
        TradeService service = new TradeServiceImpl(repo);
        Trade trade = new Trade("", "testDelete", 10, 0, TradeState.CREATED);
        service.addTrade(trade);
        service.deleteTrade(trade);

        verify(repo).delete(trade);
    }

    @Test
    public void shouldDeleteTradeByIdUsingRepoDao() {
        TradeService service = new TradeServiceImpl(repo);
        Trade trade = new Trade("", "testDeletebyID", 10, 0, TradeState.CREATED);

        ObjectId id = ObjectId.get();
        trade.setId(id);

        service.addTrade(trade);
        service.deleteTrade(id);

        verify(repo).deleteById(id);
    }
}