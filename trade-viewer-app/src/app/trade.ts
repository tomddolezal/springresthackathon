export interface Trade {
  id: string;
  date: Date;
  status: string;
  ticker: string;
  price: number;
  quantity: number;
}
// ENTITY FOR TRADE
