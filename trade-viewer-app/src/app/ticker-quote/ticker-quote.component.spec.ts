import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TickerQuoteComponent } from './ticker-quote.component';
import { HttpClientModule } from '@angular/common/http';

describe('TickerQuoteComponent', () => {
  let component: TickerQuoteComponent;
  let fixture: ComponentFixture<TickerQuoteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TickerQuoteComponent ],
      imports: [HttpClientModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TickerQuoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
