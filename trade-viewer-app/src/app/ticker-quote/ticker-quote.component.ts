import { QuoteService } from './../quote.service';
import { Trade } from './../trade';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ticker-quote',
  templateUrl: './ticker-quote.component.html',
  styleUrls: ['./ticker-quote.component.css']
})
export class TickerQuoteComponent implements OnInit {

  has_quote: any;

  @Input() set trade(trade: Trade) {
    if (trade) {
      console.log('Injection hook');
      this.quoteService.getQuoteFor(trade.ticker).subscribe((q) => {
        console.log(q.symbol);
        this.has_quote = q['Global Quote'];
      });
      console.log('hook after get');
    }
  }

  constructor(private quoteService: QuoteService) { }

  ngOnInit(): void {

  }

}
