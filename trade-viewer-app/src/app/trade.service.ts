import { Injectable } from '@angular/core';
import { Trade } from './trade';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TradeService {
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };
  private tradesUrl = 'http://docker22.conygre.com:8080/trade';

  constructor(private http: HttpClient) {}

  getTrades(): Observable<Trade[]> {
    return this.http
      .request<Trade[]>('GET', this.tradesUrl, this.httpOptions)
      .pipe(catchError(this.handleError<Trade[]>('getTrades', [])));
  }

  createTrade(trade: Trade): Observable<Trade> {
    console.log('Sending request to create new trade.');
    trade.ticker = trade.ticker.toUpperCase();
    return this.http.post<Trade>(this.tradesUrl, trade, this.httpOptions);
  }

  deleteTrade(id: string): Observable<Trade> {
    console.log('Deleting trade.');
    return this.http.delete<Trade>(this.tradesUrl + '/' + id, this.httpOptions);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error);
      // Let the app keep running by returning an empty result.
      return of(result);
    };
  }
}
