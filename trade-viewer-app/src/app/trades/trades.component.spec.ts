import { ComponentFixture, TestBed, async } from '@angular/core/testing';

import { TradesComponent } from './trades.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

describe('TradesComponent', () => {
  let component: TradesComponent;
  let fixture: ComponentFixture<TradesComponent>;
  let de : DebugElement;
  let el : HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientModule],
      declarations: [ TradesComponent ]
    })
    .compileComponents().then(() => {

      fixture = TestBed.createComponent(TradesComponent);
      component = fixture.componentInstance;
      de =fixture.debugElement.query(By.css('form'));
      el = de.nativeElement;
    });
  });

  // beforeEach(() => {
  //   fixture = TestBed.createComponent(TradesComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should set submitted to true', async(() => {
    component.onSubmit();
    expect(component.submitted).toBeTruthy();
  }));

  it('should call the onSubmit Method', async(() => {
    fixture.detectChanges();
    spyOn(component,'onSubmit');
    el = fixture.debugElement.query(By.css('button')).nativeElement;
    el.click()
  }));

  it('form should be invalid', async(() => {
    component.contactForm.controls['ticker'].setValue('');
    component.contactForm.controls['price'].setValue('');
    component.contactForm.controls['quantity'].setValue('');
    expect(component.contactForm.valid).toBeFalsy;
  }));

  it('form should be valid', async(() => {
    component.contactForm.controls['ticker'].setValue('TSLA');
    component.contactForm.controls['price'].setValue('34');
    component.contactForm.controls['quantity'].setValue('45');
    expect(component.contactForm.valid).toBeTruthy;
  }));
});
