import { QuoteService } from './../quote.service';
import { Component, OnInit } from '@angular/core';
import { Trade } from '../trade';
import { TradeService } from '../trade.service';
import { TickerQuoteComponent } from '../ticker-quote/ticker-quote.component';
import { distinctUntilChanged, debounceTime, map } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-trades',
  templateUrl: './trades.component.html',
  styleUrls: ['./trades.component.css'],
})
export class TradesComponent implements OnInit {
  trades: Trade[];
  selectedTrade: Trade;
  tickers: string[];
  contactForm: FormGroup;
  submitted = false;
  contact = {
    ticker: '',
    price: '',
    quantity: '',
  };

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      map((term) =>
        term.length < 1
          ? []
          : this.tickers.filter(
              (curInput) =>
                curInput.toLowerCase().indexOf(term.toLowerCase()) == 0
            )
      )
    );

  setTrades(): void {
    this.tradeService.getTrades().subscribe((trades) => (this.trades = trades));
  }

  delete(id: string) {
    this.tradeService.deleteTrade(id).subscribe((a) => {
      console.log('Deleting trade:' + id);
    });
  }

  addNewTrade(
    ticker: string,
    price: number,
    quantity: number,
    buy: boolean
  ): void {
    if (
      quantity <= 0 ||
      price <= 0 ||
      !ticker ||
      !this.tickers.includes(ticker.trim())
    ) {
      alert('Please enter a valid ticker');
      return;
    }
    ticker = ticker.trim();
    console.log('Sending BUY for:' + ticker + quantity + price);
    const generatedNewTrade = {
      ticker: ticker,
      price: price,
      quantity: buy ? quantity : -quantity,
      date: new Date(),
      status: 'CREATED',
    };

    this.tradeService
      .createTrade(generatedNewTrade as Trade)
      .subscribe((t) => console.log('Created new trade!'));
    this.createForm();
  }

  onSelect(trade: Trade): void {
    console.log('Selected');
    this.selectedTrade = trade;
  }

  constructor(
    private tradeService: TradeService,
    private quoteService: QuoteService
  ) {
    this.quoteService.getTickerList().subscribe((data) => {
      this.tickers = data;
    });
    this.createForm();
  }

  createForm(): void {
    this.contactForm = new FormGroup({
      ticker: new FormControl(this.contact.ticker, [
        Validators.required,
        Validators.maxLength(5),
      ]),
      price: new FormControl(this.contact.price, [
        Validators.required,
        Validators.min(1),
      ]),
      quantity: new FormControl(this.contact.quantity, [
        Validators.required,
        Validators.min(1),
      ]),
    });
  }

  ngOnInit() {
    this.setTrades();
    setInterval(() => this.setTrades(), 3000);
  }

  onSubmit(): void {
    this.submitted = true;
  }
}
