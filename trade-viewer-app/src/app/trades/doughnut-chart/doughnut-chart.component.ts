import { Component, OnInit, Input } from '@angular/core';
import { Trade } from 'src/app/trade';
@Component({
  selector: 'app-doughnut-chart',
  templateUrl: './doughnut-chart.component.html',
  styleUrls: ['./doughnut-chart.component.css'],
})
export class DoughnutChartComponent implements OnInit {
  public doughnutChartLabels: string[];
  public doughnutChartData: number[] = [500, 300, 85, 175];
  public doughnutChartType: string = 'doughnut';
  public donutColors = [
    {
      backgroundColor: [
        'rgba(110, 114, 20, 1)',
        'rgba(118, 183, 172, 1)',
        'rgba(0, 148, 97, 1)',
        'rgba(129, 78, 40, 1)',
        'rgba(129, 199, 111, 1)',
      ],
    },
  ];
  has_quote: any[];

  @Input() set trades(trades: Trade[]) {
    if (trades) {
      let flags: Set<string> = new Set<string>();
      let newPlaces: string[] = trades
        .filter((entry) => {
          if (flags.has(entry.ticker)) {
            return false;
          }
          flags.add(entry.ticker);
          return true;
        })
        .map((trade) => trade.ticker);

      let tickersTrades = [];

      newPlaces.forEach((tickName) => {
        const numTrades = trades.filter((trade) => trade.ticker === tickName)
          .length;
        tickersTrades.push(numTrades);
      });
      this.doughnutChartLabels = newPlaces;
      this.doughnutChartData = tickersTrades;
    }
  }

  constructor() {}

  ngOnInit(): void {}
}
