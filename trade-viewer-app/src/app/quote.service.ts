import { Injectable } from '@angular/core';
import { Trade } from './trade';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class QuoteService {
  private apiKey: string = '7H38DJ8F6THR51XG';
  private quoteUrl: string =
    'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=';
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  getTickerList(): Observable<any> {
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json');
    return this.http.get('assets/tickers.json', { headers });
  }

  getQuoteFor(ticker: string): Observable<any> {
    //  TODO: replcae with proper call with api key
    const globalQuoteUrl = this.quoteUrl + ticker + '&apikey=' + this.apiKey;
    console.log('Sending http get request for ticker ' + ticker);
    return this.http
      .request('GET', globalQuoteUrl, this.httpOptions)
      .pipe(catchError(this.handleError('getQuote', [])));
  }

  constructor(private http: HttpClient) {}

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error);
      // Let the app keep running by returning an empty result.
      return of(result);
    };
  }
}
