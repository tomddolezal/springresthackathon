import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { TradesComponent } from './trades/trades.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { ChartsModule } from 'ng2-charts';
import { DoughnutChartComponent } from './trades/doughnut-chart/doughnut-chart.component';
import { TickerQuoteComponent } from './ticker-quote/ticker-quote.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    TradesComponent,
    PortfolioComponent,
    DoughnutChartComponent,
    TickerQuoteComponent,
  ],

  imports: [
    HttpClientModule,
    BrowserModule,
    ChartsModule,
    FormsModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
