import { TestBed } from '@angular/core/testing';

import { TradeService } from './trade.service';
import { HttpClientModule } from '@angular/common/http';

describe('TradeService', () => {
  let service: TradeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    });
    service = TestBed.inject(TradeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
