package com.conygre.training.tradesimulator.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {

    @Id
    private ObjectId id;

    private String date;
    private String ticker;
    private int quantity;
    private int price;
    private TradeState status = TradeState.CREATED;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }


    public Trade(String date, String ticker, int quantity, int price, TradeState status) {
        this.date = date;
        this.ticker = ticker;
        this.quantity = quantity;
        this.price = price;
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public boolean setDate(String date) {
        this.date = date;
        return true;
    }

    public String getTicker() {
        return ticker;
    }

    public boolean setTicker(String ticker) {
        this.ticker = ticker;
        return true;
    }

    public int getQuantity() {
        return quantity;
    }

    public boolean setQuantity(int quantity) {
        this.quantity = quantity;
        return true;
    }

    public int getPrice() {
        return price;
    }

    public boolean setPrice(int price) {
        this.price = price;
        return true;
    }
    
    public TradeState getStatus() {
        return status;
    }

    public void setStatus(TradeState state) {
        this.status = state;
    }


}